#!/bin/bash
echo -n "" > emp_a.txt
echo -n "" > dbl_a.txt
for elem in $(ls ./OutPuts ) #on considere tous les fichiers images ds outputs
do
  fic=""
  emp=""
  dbl=0
  echo $(shasum --tag ./OutPuts/$elem |cut '-d ' -f2-4 |cut -d= -f1|sed 's/(//g;s/)//g') > temp.txt #chemin fic
  emp=$(shasum --tag ./OutPuts/$elem |cut '-d ' -f2-4 |cut -d= -f2)
  for empt in $(cat emp_a.txt)
  do
    if [ $empt = $emp ]
    then
      echo $(cat temp.txt)":"$emp |sed 's/ //g' >> dbl_a.txt
      dbl=1
    fi
  done
  if [ $dbl = 0 ]
  then
    echo $emp >> emp_a.txt
  fi
done
